# xbar-change-wallpaper

Xbar (https://xbarapp.com) plugin to change Mac OS X desktop wallpaper.

This plugin written in BASH and uses osascript, jq, curl and downloads random image from https://unsplash.com. Image category and dimentions configurable.

Put python script into $HOME/Library/Application Support/xbar/plugins folder then click Refresh from any xbar logo on menu bar or restart xbar.
