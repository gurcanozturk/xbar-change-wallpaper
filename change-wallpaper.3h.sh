#!/usr/bin/env bash
#
#  <xbar.title>Wallpaper Changer</xbar.title>
#  <xbar.version>v1.0</xbar.version>
#  <xbar.author>A.Gurcan Ozturk</xbar.author>
#  <xbar.author.github>gurcanozturk</xbar.author.github>
#  <xbar.desc>Downloads random image by search from Unsplash website and changes desktop wallpaper </xbar.desc>
#  <xbar.image>http://www.hosted-somewhere/pluginimage</xbar.image>
#  <xbar.dependencies>bash</xbar.dependencies>
#  <xbar.abouturl>http://gurcanozturk.com/</xbar.abouturl>

camera_icon="iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAACXBIWXMAAA7EAAAOxAGVKw4bAAAFjklEQVRYw61XXUgVWRz/nTPf13vNaC2syJTdfYjauObdh6KHXkKwDHchSDH6eCnIhxAsl6iHInuNguhFaYm2j4cwoc3ElJBAFO0henAp62LUXkOt9c7c+Thz9iFnuHfuXFPrwMBw5sz5/f5f5/z+BCHj2rVrUl1dXb2iKL8B+BXAOgAyljZsAP8CGHVdt2tgYOD2/v379eAiEpx49+7dXlmWLzuOU2HbNhhj4Jx/WUzIopC99QAgCAIkSYIkSSkAbaWlpR0FCUxNTV1ijLUahkEYY6CU+k82+EJEPHDOOVzX9R9KKTRNgyiKN3t7e480NDTYOQRSqdRF27bbDMMAIQSiKEIURR98sdZnE/FIMMbgOA4YY9A0DbIs31q9enWjT+DVq1c1qqo+1HWdCIIAWZZ98KD1SyXhecBxHNi2DcdxoGkaBEE4VlZWdl08f/68oGnaZV3XCaUUkiRBFEUIgpADvhwSnHPfe97/ruvCMAwUFRVdvH///l/k5cuXe6LRaHcmk4GiKDnWL8f1C4XCcRxYlgXLsiBJEmRZbhaLi4vrDcMApTTP6mBGg3MkOzowOzwMZM/PD628HBXNzRBjsdxSmzckG8OyLIiiWC+6rlvNGIMsy1+1NtXTg4krVwp+nxkaAgjBT21toZXhkfAIMMaqRQBlXqzyTpLPn6FPTACuCwCY7OzE1wKS6urCD7t2gSoKQAjU9eshr1oV6hHOeTFJJpMzuq6XBOM/3d+Pf86ehWsY35QDRBBQ3tyMtU1NOXlgmiYURQFJJpMz6XS6RFXVHAKjtbWwUyl8lyGKSPT3A7Lsl6NHQAxmq/fANEEoDd+vpASldXUo2rwZhFLo4+OYevAA1ocP4QRcF65tg0hSDhaALwSyM937SAkBDcmLWHU1Ki9cgFBcDO44gOtixY4dWNPQgGR7O6YfPw4vx/kq4oHqKegBEkJAXrsWlZcugTsO3p47h9mBAcB1EUsksL61FeVnzsB+/x7pFy/CDoRQHDFsknMOSil4IARrmppAVBVvT57E3OioXxFzw8OYaGnBj52dWHP0KN60tIRfUsEwB0OQQ4CQPAJFVVUw37yB/vw5aOCbPTmJuZERRLdtgyBJ4IwtjUBeCAQhLwlpJAIzlSqYnGx2FhBFUFXNK1+eFYZso/MI+PVLKagg5GxiTk5CrqyEEImAm2ag4AnUTZtgf/wIWFbev4vyQPY7oTTP0v/6+lB64gRKjx/H1NWr/gkJACsPHIC0bh1m7t4N91AI+MIhCCPQ24ui7dsR270bckUF0oOD4IwhkkhA27oV5uvX+HTvXigBHiDhzpMXw8ALEQCAVHs7VjY2IlpTg5WHDs0Hn2HuyRNMd3SAW1Y4gQVCYC+FABwHMzdu4NOdO5A2bgShFFYyCXduzs+dgloxBEc0TfM9gNJs8cg5R2TnTqR7egoLDcuCNT6ek7SFhhqPgygK3Cz3z19Mn8V0Oj3iuu4vjDG4rusvWHH4MKQNG2A8e/blyF3OTUgplC1bEN23D26WRvSwdF0fIX19fXtisVi3J5slSYIgCN9FjoXJMtu2kclkYNs2JElqFru7u/8+ePDguK7rP2drweBJtxxZHlTH3lVsGAY0TZseGhr6kwDA06dPa1RVfWjbNolGo74XvlWYZieb4zhwHAfpdBqEEMiyfKyqquq6v/PY2NhF0zTbbNtGLBbzpXlQVi+nL/AaEw9c07Rb8Xi8Mec6jsfjf4yNjVFKaevs7CxRVRWqquZ4Yam9oZftpmnCMAyoqgpFUW4+evToSMHmdHBwcG8kErmcyWQqLMvyr+bsnFhMb+iBE0IgSRI0TUsxxtoSiUTHgt0xAJw6dUqqra2tj8Viv3POE5zz5bTnDiHkAyFk1LKsrv7+/tunT5/Oa8//B8FTvlKAm+GPAAAAAElFTkSuQmCCMTUwMA=="

# Your own access key for Unsplash API
ACCESS_KEY=""

WP_PATH="/tmp/"
WP_FILE="wallpaper.jpg"

RANDOM_LINK="https://api.unsplash.com/photos/random?client_id=${ACCESS_KEY}&count=1"
IMAGE_LINK=$(/usr/bin/curl -s ${RANDOM_LINK} | jq -r '.[] | .links.download')

curl -s --location ${IMAGE_LINK} --output-dir ${WP_PATH} --output ${WP_FILE}

/usr/bin/osascript -e 'tell application "System Events" to tell every desktop to set picture to "${WP_PATH}/${WP_FILE}"'
#/usr/bin/killall Dock


echo " | image=$camera_icon"
echo "---"
echo "Change desktop wallpaper | bash='$0' param1=change terminal=false refresh=true"
exit